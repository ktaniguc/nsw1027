git atlas init-workdir https://:@gitlab.cern.ch:8443/yesumi/athena.git
cd athena
git fetch upstream
git checkout -b 21.3-yesumi_v2 upstream/21.3 --no-track
git atlas addpkg TrigL2MuonSA
cd -
echo "+ Trigger/TrigAlgorithms/TrigL2MuonSA" > package_filters.txt
