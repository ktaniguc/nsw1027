#! /bin/sh

# pwd
# >> hoge/source
# ./compile.sh cmake
# >> try compile with cmake

if [ "$TestArea" = "" ] ; then 
  echo "TestArea is missing"
  exit 1
fi
echo "TestArea = "$TestArea

BUILD_DIR=$TestArea/build
if [ -d "$BUILD_DIR" ] ; then
  echo "Directory for build is $BUILD_DIR"
else
  echo "Creating $BUILD_DIR ..." && mkdir -p $BUILD_DIR
fi

cd $BUILD_DIR
if [ $# -gt 0 ] && [ $1 = "cmake" ]; then
  cmake -DATLAS_PACKAGE_FILTER_FILE= ../package_filters.txt ../athena/Projects/WorkDir
fi
make -j4
cd -
